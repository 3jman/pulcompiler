#include <stdio.h>
#include <stdlib.h>

size_t getLineFromStdin(char **lineptr, size_t *n) {
    ssize_t read = getline(lineptr, n, stdin);
    if (read == -1) {
        return 0;
    }
    return read;
}

extern void *malloc;
extern void free;
extern void *memcpy;
extern int puts;
