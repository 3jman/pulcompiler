{-# LANGUAGE OverloadedStrings #-}
module CFns where

import LLVM.IRBuilder.Module
import LLVM.AST.Operand
import LLVM.AST.Type

malloc :: ModuleBuilder Operand
malloc = extern "malloc" [i32] (ptr i8)

free :: ModuleBuilder Operand
free = extern "free" [(ptr i8)] void

memcpy :: ModuleBuilder Operand
memcpy = extern "memcpy" [ptr i8, ptr i8, i64] (ptr i8)

getline :: ModuleBuilder Operand
getline = extern "getLineFromStdin" [(ptr $ ptr i8), (ptr i32)] i32

puts :: ModuleBuilder Operand
puts = extern "puts" [ptr i8] i32
