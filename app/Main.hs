module Main where

import Defs
import Checks
import Pars
import Text.Parsec.String (parseFromFile)
import System.Environment (getArgs)
main :: IO ()
main = do
  l <- getArgs
  case length l of
    1 -> do
      parsed <- parseFromFile pulParse (head l)
      case parsed of
        Left err  -> print err
        Right pul -> nameCheck pul { hints = CompilerHints { maxStrSize = 666, preAllocSize = 666 }, name = "the beast" }
    _ -> error "WTF????"
