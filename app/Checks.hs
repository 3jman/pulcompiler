module Checks where

import Defs
import Control.Monad.Trans.Reader
import qualified Data.Map as Map
import qualified Data.Set as Set
import Control.Monad
import Control.Exception (throw)
import Control.Monad.Trans.Class (lift)
import Data.Maybe (fromMaybe)

isolateIdent :: Term -> [Ident]
isolateIdent (Var id)    = [id]
isolateIdent (Cond _ id) = [id]
isolateIdent (Cat i1 i2) = [i1,i2]
isolateIdent _          = []

type CallChecker = Map.Map Ident (Maybe [Ident]) --If value is Nothing, key has duplicates


getNames :: Pul -> CallChecker
getNames pul = Map.fromListWith (\d1 d2 -> Nothing)
               (zip refs calls)
  where
    bs    = blocks pul
    refs  = map ref bs
    ts    = map terms bs
    ids   = map (\lts -> map isolateIdent lts) ts
    calls = map (\l -> Just $ foldl1 (++) l) ids
        
checkCalls :: ReaderT CallChecker Maybe [Ident]
checkCalls = do
  names <- ask
  let namesC = (Map.filter (\m -> case m of
                               Nothing -> True
                               _       -> False) names)
    in
    case namesC == Map.empty of
      True  -> lift Nothing
      False -> return $ Map.keys namesC
  

duplicatesCheck :: ReaderT Pul Maybe [Ident]
duplicatesCheck = withReaderT getNames checkCalls

checkIfExists :: ReaderT CallChecker Maybe [Ident]
checkIfExists = do
  names <- ask
  let helper = (Set.fromList (foldl1 (++) (map (fromMaybe []) (Map.elems names)))) Set.\\ (Set.fromList (Map.keys names)) in
    case (helper == Set.empty) of
    True  -> lift Nothing
    False -> return $ Set.toList helper

existenceCheck :: ReaderT Pul Maybe [Ident]
existenceCheck = withReaderT getNames checkIfExists
    
nameCheck :: Pul -> IO ()
nameCheck pul = case (runReaderT duplicatesCheck pul, runReaderT existenceCheck pul) of
  (Just list, Just list2) -> handleDupl list
                             >> handleExis list2
  (Just list, Nothing)    -> handleDupl list
                             >> putStrLn ("All calls point to existing references in " ++ (name pul))
  (Nothing, Just list2)   -> putStrLn ("No duplicate references exist in " ++ (name pul))
                             >> handleExis list2
  (Nothing, Nothing)      -> putStrLn ("No duplicate references exist in " ++ (name pul))
                             >> putStrLn ("All calls point to existing references in " ++ (name pul))
handleDupl l = if length l > 1
               then throw $ ListOfErrs $ map ReferenceDuplicate l
               else throw $ ReferenceDuplicate $ head l
handleExis l = if length l > 1
               then throw $ ListOfErrs $ map ReferenceDoesNotExist l
               else throw $ ReferenceDoesNotExist $ head l
