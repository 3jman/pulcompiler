{-# LANGUAGE OverloadedStrings #-}
module CodeGen where

import Defs
import Types
import String
import RWMachine
import LLVM.AST.Constant as Const
import LLVM.IRBuilder.Module as M
import Control.Monad.Trans.State
import Control.Monad.Trans.Reader
import Data.Map.Strict as Map


{-
NULL   = 000
VAR    = 001
ACT    = 010
ESC    = 011
COND   = 100
CONCAT = 101
ILEFT  = 110
IRIGHT = 111
TRNULL = 1000
-}


builtInExts = Map.fromList $ zip ["malloc", "free", "memcpy", "getline", "puts"] [malloc, free, memcpy, getline, puts]
--builtInFuns = ["init_string", "delete_string", "string_resize", "string_add_char", "string_concat", "string_eq", "string_search", "tailNth", "length_str_array", "rewrite"]
builtIns = [stringDelete, stringResize, stringAddChar, stringConcat, stringEquality, stringSearch, tailNth, rewrite]

-- builtInHelps r = Map.fromList $ zip builtInFuns builtIns
--   where builtIns  = builtInExts `Map.union` inits `Map.union` rest   
--         handle s l = case l of
--           (x:xs) -> (runStateT x s) >>=  ->  handle s' xs
--           []     -> []
--         env        = CompEnv { builtInExternals = builtInExts, builtInHelpers = M.empty }
--         inits      = Map.fromList $ zip ["init_string", "length_str_array"] (map (\x -> runReaderT x r) [initString, lengthStrArray])
--         rest       = (handle env builtIns) >>= \s' -> builtInExternals s'
          
                       
buildTermWith :: Integer -> Term -> Constant --THE INTEGER DENOTES A COMPILER HINT-- --IMPORTANT!!!-- --IT IS AN ADMISSIBLE COMPILER HINT, it can be inferred before compile time, and is called maxStrSize
buildTermWith n (Defs.Null b)  = case b of
  False -> termHelper [mkInt 8 0, mkString n "0", mkString n "0"]
  True  -> termHelper [mkInt 8 8, mkString n "1", mkString n "1"]
buildTermWith n (Var id)       = termHelper [mkInt 8 1, mkString n id, mkString n "V"]
buildTermWith n (Act (fr, to)) = termHelper [mkInt 8 2, mkString n fr, mkString n to]
buildTermWith n (Esc str)      = termHelper [mkInt 8 3, mkString n str, mkString n "E"]
buildTermWith n (Cond str id2) = termHelper [mkInt 8 4, mkString n str, mkString n id2]
buildTermWith n (Cat id1 id2)  = termHelper [mkInt 8 5, mkString n id1, mkString n id2]
buildTermWith n (Internal fl)  = case fl of
  ConcatL -> termHelper [mkInt 8 6, mkString n "C", mkString n "L"]
  ConcatR -> termHelper [mkInt 8 7, mkString n "C", mkString n "R"]

termHelper l = Struct { Const.isPacked = False, structName = Nothing, memberValues = l }

buildTerm :: Term -> Reader CompilerHints Constant
buildTerm t = do
  n <- asks maxStrSize
  return $ buildTermWith n t
  
buildBlock :: Block -> ReaderT CompilerHints ModuleBuilder Operand
buildBlock bl = do
  compHints <- ask
  let primItemU = runReader primItem compHints
      buildTermU = \t -> runReader (buildTerm t) compHints
    in
  return $ global
    (ref bl)
    ArrayType { nArrayElements = w64 $ length $ terms bl, elementType = primItemU }
    Array { memberValues = map buildTermU (terms bl), memberType = primItemU }

-- build :: Pul -> Module
-- build pul =   

