{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}
module String where

import Types
import CFns
import LLVM.AST.IntegerPredicate as I
import LLVM.AST.Type as Type
import LLVM.AST.Operand
import LLVM.IRBuilder.Monad
import LLVM.IRBuilder.Module as M
import LLVM.IRBuilder.Instruction
import LLVM.IRBuilder.Constant
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.State
import Control.Monad.Trans.Reader
import Data.Map.Strict as Map



  
initString :: ReaderT CompilerHints ModuleBuilder Operand
initString = do
  r <- ask
  M.function "init_string"
             [(ptr string, "pointerToString")]
             void
             (\[p] -> runReaderT (initStringBody p) r)

initStringBody :: Integer -> Operand -> ReaderT IRBuilderT ModuleBuilder ()
initStringBody p = do
  n       <- asks preAllocSize
  bufferP <- gep p [(int32 0),(int32 0)]
  store bufferP 1 charNull
  lengthP <- gep p [(int32 0),(int32 1)]
  store lengthP 1 (int64 0)
  maxlenP <- gep p [(int32 0), (int32 2)]
  store maxlenP 1 (int64 0)
  factorP <- gep p [(int32 0), (int32 3)]
  store factorP 1 (int64 $ fromIntegral n)
  
  retVoid

  
stringDelete :: StateT CompilerEnv ModuleBuilder Operand
stringDelete = do
  state <- get
  op    <- M.function "delete_string"
           [(ptr string, "pointerToString")]
           void
           (\[p] -> evalStateT (stringDeleteBody p) state)
  modify (\s -> s { builtInHelpers = (builtInHelpers s) `Map.union` (Map.singleton "delete_string" op) })
  return op
          
               
stringDeleteBody :: Operand -> StateT CompilerEnv IRBuilderT ModuleBuilder ()
stringDeleteBody p = mdo
  bufferP <- gep p [int32 0, int32 0]
  lineptr <- load bufferP 1
  char    <- load lineptr 1
  bool    <- icmp I.NE char charNull --THIS PRESUPPOSES THAT STRINGS ALWAYS END WITH \0--
  condBr bool beginB endB
    
  beginB  <- block `named` "free_begin"
  ptrToFree <- gets builtInExternals Map.! "free"
  call ptrToFree [(lineptr, [])]
  br closeB
                   
  closeB  <- block `named` "free_close"
  retVoid



stringResize :: StateT CompilerEnv ModuleBuilder Operand
stringResize = do
  state <- get
  op <- M.function "string_resize"
        [(ptr string, "pointerToString"), (i64, "sizeOfResize")]
        void
        (\[p, siz] -> evalStateT (stringResizeBody p siz) state)
  modify (\s -> s { builtInHelpers = (builtInHelpers s) `Map.union` (Map.singleton "string_resize" op) })
  return op
  
stringResizeBody :: Operand -> Operand -> StateT IRBuilderT ModuleBuilder () --it only makes it bigger, NOT smaller!
stringResizeBody p siz =  do                                                 --but no checks are run to make it safe
  mallocR <- gets builtInExternals Map.! "malloc"
  output  <- call mallocR [(siz, [])]
  bufferP <- gep p [int32 0, int32 0]
  buffer  <- load bufferP 1
  lengthP <- gep p [int32 0, int32 1]
  length  <- load lengthP 1
  memcpyR <- gets builtInExternals Map.! "memcpy"
  call memcpyR (zip [output, buffer, length] (repeat []))
  freeR   <- getsBuiltInExternals Map.! "free"
  call freeR [(bufferP, [])]
  store output 1 bufferP
  maxlenP <- gep p [int32 0, int32 2]
  store maxlenP 1 siz
  retVoid

  
stringAddChar = do
  state <- get
  op <- M.function "string_add_char"
        [(ptr string, "pointerToString"), (i8, "char")]
        void
        (\[p, ch] -> evalStateT (stringAddCharBody p ch) state)
  modify (\s -> s { builtInHelpers = (builtInHelpers s) `Map.union` (Map.singleton "string_add_char" op) })
  return op
  
stringAddCharBody :: Operand -> Operand -> StateT CompilerEnv IRBuilderT ModuleBuilder ()
stringAddCharBody p ch = mdo
  lengthP <- gep p [int32 0, int32 1]
  length  <- load lengthP 1
  maxlenP <- gep p [int32 0, int32 2]
  maxlen  <- load maxlenP 1
  bool    <- icmp I.EQ length maxlen
  condBr bool beginB endB
  
  beginB  <- block `named` "grow_begin"
  factorP <- gep p [int32 0, int32 3]
  factor  <- load factorP 1
  newsize <- add factor maxlen
  strResR <- gets builtInHelpers Map.! "string_resize"
  call strResR (zip [p, newsize] (repeat []))
  br endB

  endB    <- block `named` "grow_end"
  bufferP <- gep p [int32 0, int32 0]
  buffer  <- load bufferP 1
  lastpos <- gep buffer [length]
  store lastpos 1 ch
  newlen  <- add length (int64 1)
  nullTrm <- gep buffer [newlen]
  store nullTrm 1 (int8 0)
  store lengthP 1 newlen

  retVoid

stringConcat = do
  state <- get
  op <- M.function "string_concat"
        [(ptr string, "pointer1"), (ptr string, "pointer2")]
        void
        (\[p1, p2] -> evalStateT (stringConcatBody p1 p2) state)
  modify (\s -> s { builtInHelpers = (builtInHelpers s) `Map.union` (Map.singleton "string_concat" op) })
  return op
  

stringConcatBody p1 p2 = mdo
  lengthP2 <- gep p2 [int32 0, int32 1]
  length2  <- load lengthP2 1
  bool     <- icmp I.NE length2 (int64 0)
  condBr bool execB endB

  execB    <- block `named` "execute_concat"
  strResR  <- gets builtInHelpers Map.! "string_resize"
  lengthP1 <- gep p1 [int32 0, int32 1]
  length1  <- load lengthP1 1
  nLength  <- add length1 length2
  call strResR (zip [p1, nLength] (repeat []))
  bufferP  <- gep p1 [int32 0, int32 0]
  buffer   <- load bufferP 1
  startPos <- add (int64 1) length1
  lastposP <- gep buffer [startPos]
  bufferP2 <- gep p2 [int32 0, int32 0]
  memcpyR  <- gets builtInExternals Map.! "memcpy"
  call memcpyR (zip [lastposP, bufferP2, length2] (repeat []))
  br endB

  endB     <- block `named` "end_concat"
  retVoid

 
  
  
stringEquality = do
  state <- get
  op    <- M.function "string_eq"
           [(ptr string, "left_side"), (ptr string, "right_side")]
           i1
           (\[p1, p2] -> evalStateT (stringEqualityBody p1 p2) state)
  modify (\s -> s { builtInHelpers = (builtInHelpers s) `Map.union` (Map.singleton "string_eq" op) })

stringEqualityBody p1 p2 = mdo
  preB     <- block `named` "pre_header"
  zeroP    <- alloca i64 (Just $ int32 1) 1 --this is not optimal, better for it to be a universal constant
  store zeroP 1 (int64 0)
  zero     <- load zeroP 1
  leftP    <- gep p1 [int32 0, int32 0]
  rightP   <- gep p1 [int32 0, int32 0]
  lengthP1 <- gep p1 [int32 0, int32 1]
  length1  <- load lengthP1 1
  lengthP2 <- gep p2 [int32 0, int32 1]
  length2  <- load lengthP2 1
  bool     <- icmp I.EQ length1 length2
  condBr bool execB endB

  execB    <- block `named` "exec"
  i        <- phi [(zero, preB), (step, execB)]
  chLeft   <- gep leftP [i]
  chRight  <- gep rightP [i]
  step     <- add (int64 1) i
  comp     <- icmp I.EQ chLeft chRight
  bound    <- icmp I.UGT length1 step
  bool2    <- and comp bound
  condBr comp execB endB 

  endB     <- block `named` "end"
  bool3    <- icmp I.EQ length1 step
  condBr bool3 endTrue endFals
  
  endTrue <- block `named` "end_true"
  ret (int1 1)

  endFals <- block `named` "end_false"
  ret (int1 0)
  



stringSearch = do
  state <- get
  op    <- M.function "string_search" --THIS FUNCTION assumes that the cursor is initialized
           [(ptr string, "targetOfSearch"), (ptr cursor, "cursorOp")]
           i1
           (\[pS, pC] -> evalStateT (stringSearchBody pS pC) state)
  modify (\s -> s { builtInHelpers = (builtInHelpers s) `Map.union` (Map.singleton "string_search" op) })
  return op

stringSearchBody pS pC = mdo
  headB      <- block `named` "head"
  stepP      <- gep pS [int32 0, int32 1]
  step       <- load stepP 1
  traversedP <- gep pC [int32 0, int32 1]
  traversed  <- load traversedP 1
  next       <- add step traversed
  faceP      <- gep pC [int32 0, int32 0] --is a pointer to a pointer to a string
  face       <- load faceP 1              --is a pointer to string
  lengthP    <- gep face [int32 0, int32 1]
  length     <- load lengthP 1
  bool       <- icmp I.UGT next length
  condBr bool endB contB

  contB      <- block `named` "cont"
  ptrToEq    <- gets builtInHelpers Map.! "string_eq"
  ptrToInit  <- gets builtInHelpers Map.! "init_string"
  ptrToAdd   <- gets builtInHelpers Map.! "string_add_char"  
  tempP      <- alloca string (Just $ int32 1) 1
  counter    <- alloca i64 (Just $ int32 1) 1
  store counter 1 (int64 0)
  ctr        <- load counter 1
  call ptrToInit [(tempP, [])]
  br writeB

  writeB     <- block `named` "write"
  currPos    <- phi [(traversed, headB), (position, writeB)]
  i          <- phi [(ctr, contB), (incr, writeB)]
  position   <- add (int64 1) currPos
  charP      <- gep face [int32 0, position]
  char       <- load charP 1
  call ptrToAdd [(tempP, []), (char, [])]
  incr       <- add (int64 1) i
  counterEnd <- icmp I.UGE incr step
  nextPos    <- add (int64 1) position
  reachedEnd <- icmp I.EQ nextPos length
  bool2      <- or counterEnd reachedEnd
  condBr bool2 compB writeB

  compB      <- block `named` "compare"
  areEq      <- call ptrToEq tempP pS
  condBr areEq succB writeB

  succB      <- block `named` "success"
  ret (int1 1)

  endB       <- block `named` "end"
  ret (int1 0)
  
  
tailNth =  do
  state <- get
  op    <- M.function "tailNth"
           [(ptr string, "inputString"), (i64, "size")]
           void
           (\[inp, siz] -> evalStateT (tailNthBody inp siz) state)
  modify (\s -> s { builtInHelpers = (builtInHelpers s) `Map.union` (Map.singleton "tailNth" op) })
  return op
  
tailNthBody inp siz = do
  headB  <- block `named` "head"
  sizeP  <- gep inp [int32 0, int32 1]
  strSiz <- load sizeP 1
  bool   <- icmp I.UGE strSiz siz
  oldP   <- gep inp [int32 0, int32 0]
  free   <- gets builtInExternals Map.! "free"
  condBr bool contB failB

  contB  <- block `named` "cont"
  memcpy <- gets builtInExternals Map.! "memcpy"
  malloc <- gets builtInExternals Map.! "malloc"
  arrPtr <- call malloc [(siz, [])]
  locat  <- gep inp [int32 0, int32 0, siz]
  diff   <- sub strSiz siz
  call free [(sizeP, [])]
  call memcpy [(arrPtr, []), (locat, []), (diff, [])]
  store diff 1 sizeP
  call free [(oldP, [])]
  store arrPtr 1 oldP
  br endB

  failB  <- block `named` "fail"
  call free [(oldP, [])]
  init   <- gets builtInHelpers "init_string"
  call init [(inp, [])]
  retVoid

  endB   <- block `named` "end"
  retVoid
  
headNth = --hmmm
