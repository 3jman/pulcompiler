module PulMain where

import LLVM.AST.IntegerPredicate as I
import LLVM.AST.Type as Type
import LLVM.AST.Operand
import LLVM.IRBuilder.Monad
import LLVM.IRBuilder.Module as M
import LLVM.IRBuilder.Instruction
import LLVM.IRBuilder.Constant
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.State
import Control.Monad.Trans.Reader
import Data.Map.Strict as Map

