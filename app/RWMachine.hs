module RWMachine where

import String
import Types
import Data.Map.Strict as Map
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.State
import Control.Monad.Trans.Reader
import LLVM.AST.IntegerPredicate as I
import LLVM.AST.Type as Type
import LLVM.AST.Operand
import LLVM.IRBuilder.Monad
import LLVM.IRBuilder.Module as M
import LLVM.IRBuilder.Instruction
import LLVM.IRBuilder.Constant

lengthStrArray = do
  M.function "length_str_array"
    [(runReader stringOfSize reads, "inputArray")]
    i64
    (\[inp] -> lengthStrArrayBody inp)

lengthStrArray input = mdo
  headB <- block `named` "head"
  first <- extractValue input [int32 0]
  bool  <- icmp I.EQ first (int8 0)
  count <- int64 1
  condBr bool empty contB

  contB <- block `named` "cont"
  i     <- phi [(count, headB), (step, contB)]
  next  <- extractValue input [i]
  step  <- add (int64 1) i
  comp  <- icmp I.EQ next (int8 0)
  condBr comp endB contB

  empty <- block `named` "empty"
  ret (int64 0)

  endB  <- block `named` "end"
  ret i
  
rewrite :: StateT CompilerEnv (ReaderT CompilerHints ModuleBuilder) Operand
rewrite = do
  state <- get
  reads <- lift ask
  op    <- M.function "rewrite"
           [(ptr str, "input"), (runReader stringOfSize reads, "start"), (runReader stringOfSize reads, "finish")]
           void
           (\[str, start, fin] -> evalStateT (rewriteBody str start fin) state)
  modify (\s -> s { builtInHelpers = (builtInHelpers s) `Map.union` (Map.singleton "rewrite" op) })

rewriteBody str start fin = do
  headB   <- block `named` "head"
  --some things
  inpBuff <- gep str [int32 0, int32 0]
  inpLenP <- gep str [int32 0, int32 1]
  inpLen  <- load inpLenP 1
  memcpy  <- gets builtInExternals Map.! "memcpy"
  malloc  <- gets builtInExternals Map.! "malloc"
  --initialize cursor
  cursorP <- alloca cursor (Just $ int32 1) 1
  pointSt <- gep cursorP [int32 0, int32 0]
  store str 1 pointSt
  stepsP  <- gep cursorP [int32 0, int32 1]
  store (int64 0) 1 stepsP
  --initialize string target and output
  output  <- alloca string (Just $ int32 1) 1
  target  <- alloca string (Just $ int32 1) 1
  initStr <- gets builtInHelpers Map.! "init_string"
  call initStr [(target, [])]
  call initStr [(output, [])]
  lenFun  <- gets builtInHelpers Map.! "length_str_array"
  length  <- call lenFun [(start, [])]
  length2 <- call lenFun [(fin, [])]
  resize  <- gets builtInHelpers Map.! "string_resize"
  call resize [(target, []), (length, [])]
  call resize [(output, []), (length2, [])]
  bufferP <- gep target [int32 0, int32 0]
  buffer2 <- gep output [int32 0, int32 0]
  endP    <- gep fin [int32 0]
  startP  <- gep start [int32 0]
  store startP 1 bufferP
  store endP 1 buffer 2
  searchF <- gets builtInHelpers Map.! "string_search"
  br searchB

  searchB <- block `named` "search"
  bool    <- call searchF [(target, []), (cursorP, [])]
  condBr bool rewritB endB

  rewritB <- block `named` "rewrite"
  posOfRW <- gep cursorP [int32 0, int32 1]
  posOf   <- load posOfRW 1
  lenOf1  <- sub posOf length
  lenOf2  <- sub inpLen posOf
  ls      <- add lenOf1 lenOf2
  newsiz  <- add length2 ls
  newBuff <- call malloc [(newsiz, [])]
  call memcpy $ zip [newBuff, inpBuff, lenOf1] (repeat [])
  len1    <- add (int64 1) lenOf1
  ptr1    <- gep newBuff [len1]
  source  <- gep str [int32 0, int32 0, lenOf1]
  call memcpy $ zip [ptr1, bufferP, length] (repeat [])
  len2    <- add (int64 1) posOf
  ptr2    <- gep newBuff [len2]
  call memcpy $ zip [ptr2, buffer2, length2]
  store newBuff 1 inpBuff
  br searchB

  endB    <- block `named` "end"
  retVoid

  
  
  
  
  
  
