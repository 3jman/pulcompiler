module Defs where

import Control.Exception
import Data.Map.Strict
import LLVM.AST.Operand

data Block = Block { ref :: Ident, terms :: [Term] } deriving Show

data Term = Var Ident
  | Act (String, String)
  | Esc String
  | Null Bool
  | Cond String Ident
  | Cat Ident Ident
  | Internal Flag
  deriving Eq

data Flag = ConcatL | ConcatR deriving (Show, Eq)
type Ident = String

data Pul = Pul { blocks :: [Block], run :: Run, hints :: CompilerHints, name :: String } deriving Show

data CompilerHints = CompilerHints { maxStrSize :: Integer, preAllocSize :: Integer } deriving Show
data Run = Run Ident | Debug Ident deriving (Show, Eq)
getter (Run s) = s
getter (Debug s) = s

data PulExceptions = ReferenceDoesNotExist String
                   | ReferenceDuplicate String
                   | OutOfBoundsStringCreation String
                   | ListOfErrs [PulExceptions]
                   deriving Show

instance Exception PulExceptions

data CompilerEnv = CompilerEnv { builtInExternals :: Map String Operand
                               , builtInHelpers :: Map String Operand
                               --, stackedPrimItems :: Map String Operand
                               }


instance Show Term where
  show (Act (a,b))  = "-" ++ a ++ "-" ++ b
  show (Var id)     = id
  show (Esc str)    = "@ " ++ str
  show (Cond s t)   = "|" ++ s ++ ">" ++ t
  show (Null b)     = "null" ++ case b of
                                  False -> ""
                                  _ -> show b
  show (Cat t1 t2)  = t1 ++ "  ++  " ++ t2
  show (Internal f) = show f    
