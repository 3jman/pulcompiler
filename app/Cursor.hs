{-# LANGUAGE OverloadedStrings #-}
module Cursor where

import Types
import LLVM.IRBuilder.Monad
import LLVM.IRBuilder.Module as M
import LLVM.AST.Type as Type
import LLVM.AST.Operand
import LLVM.IRBuilder.Monad
import LLVM.IRBuilder.Module as M
import LLVM.IRBuilder.Instruction
import LLVM.IRBuilder.Constant
import Control.Monad.Trans.Class (lift)


initCursor :: ModuleBuilder Operand
initCursor = M.function "init_cursor"
             [(ptr cursor, "pointerToCursor"), (ptr string, "pointerToString")]
             void
             (\[pC, pS] -> do
                 str <- gep pC [int32 0, int32 0]
                 store str 1 pS
                 stp <- gep pC [int32 0, int32 1]
                 store stp 1 (int64 0)
             )
                 
