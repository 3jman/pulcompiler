module Types where

import Defs
import LLVM.AST.Type as Type
import LLVM.AST.Constant
import LLVM.AST.Operand
import Data.Char (ord)
import Data.Bits.Extras (w64)
import Control.Exception (throw)
import Control.Monad.Trans.Reader


stringOfSize :: Reader CompilerHints Type
stringOfSize = do
  n <- asks maxStrSize
  return $ ArrayType { nArrayElements = fromIntegral n, elementType = i8 }

mkString :: String -> Reader CompilerHints Constant
mkString str = do
  n <- asks maxStrSize
  return $ Array { memberType = i8
                 , memberValues = if (length str > (fromIntegral n))  --THIS FUNCTION ADDS ZEROES TO ACCOMPLISH DESIRED LENGTH--
                                  then throw (OutOfBoundsStringCreation str)
                                  else let asciiVals = map (fromIntegral . ord) str in addZeros (fromIntegral n) (map (mkInt 8) asciiVals) }
    where addZeros n l = if length l == n then l else (addZeros n (l ++ [mkInt 8 0])) 

mkInt :: Integer -> Integer -> Constant                                                      
mkInt n m = Int { integerBits = fromIntegral n, integerValue = m }

charNull :: Operand
charNull = ConstantOperand $ Int { integerBits = 8, integerValue = 0 }

primItem :: Reader CompilerHints Type
primItem = do
  compHints <- ask
  return $ StructureType { Type.isPacked = False
                         , elementTypes = [ i8           --pul instruction number encoded here
                                          , runReader stringOfSize compHints
                                          , runReader stringOfSize compHints ] }

  
stack :: Reader CompilerHints Type
stack = do
  compHints <- ask  
  return $ StructureType { Type.isPacked = False
                         , elementTypes = [ ptr $ runReader primItem compHints
                                          , i64 ] } --length - 1
  
cursor :: Type
cursor = StructureType { Type.isPacked = False
                       , elementTypes = [ ptr string       --FACE OF CURSOR--
                                        , i64 ] }          --STEPS TAKEN--

string :: Type
string = StructureType { Type.isPacked = False
                       , elementTypes = [ ptr i8 -- pointer to char buffer
                                        , i64    --length of buffer
                                        , i64    --maxlength, memory allocated
                                        , i64 ] } --factor of preallocation when growing         
                                        
