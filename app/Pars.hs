module Pars where

import Defs
import Text.Parsec
import Text.Parsec.Combinator
import Text.Parsec.Char
import Text.Parsec.String
import Text.Parsec.Expr
import Text.Parsec.Language
import Text.Parsec.Token

def = emptyDef { commentStart = "-**"
               , commentEnd = "**-"
               , commentLine = "-***"
               , identStart = letter
               , identLetter = alphaNum
               , opStart = oneOf "<>@|+"
               , opLetter = oneOf "<>@|+"
               , reservedOpNames = ["<", ">", "@", "|", "+"]
               , reservedNames = ["run", "debug", "null", "trueNull"]
               , caseSensitive = True
               }

TokenParser { parens = m_parens
            , identifier = m_identifier
            , reservedOp = m_reservedOp
            , reserved = m_reserved
            , whiteSpace = m_whiteSpace
            , semi = m_semi
            , comma = m_comma
            , stringLiteral = m_stringLiteral
            , charLiteral = m_charLiteral
            , commaSep1 = m_commaSep1
            , symbol = m_symbol
            } = makeTokenParser def
        

termParse :: Parser Term
termParse = (try ruleParse)
  <|> (try escParse)
  <|> (try nullParse)
  <|> (try condParse)
  <|> (try catParse)
  <|> (Var <$> try m_identifier)
catParse :: Parser Term
catParse = do
  id1 <- m_identifier
  m_reservedOp "+"
  id2 <- m_identifier
  return $ Cat id1 id2

condParse :: Parser Term
condParse = do
  m_reservedOp "|"
  inp <- manyTill (noneOf ",@- ") (try $ lookAhead $ m_reservedOp ">")
  id <- m_identifier
  return $ Cond inp id

nullParse :: Parser Term
nullParse = ((try $ m_reserved "null")
             >> (return $ Null False))
            <|> ((m_reserved "trueNull") >> (return $ Null True))

ruleParse :: Parser Term
ruleParse = do
  m_symbol "-"
  target <- many1 ((noneOf ",@- ") <|> m_charLiteral) <* m_symbol "-"
  output <- manyTill ((noneOf ",@-") <|> m_charLiteral) (try $ lookAhead $ m_comma <|> m_semi)
  return $ Act (target, output)

escParse :: Parser Term
escParse = do
  m_reservedOp "@"
  str <- m_stringLiteral
  return $ Esc str

listParse :: Parser [Term]
listParse = m_reservedOp "<" *> (m_commaSep1 termParse) <*  m_semi

blockParse :: Parser Block
blockParse = do
  m_reservedOp ">"
  name <- m_identifier
  trms <- listParse
  option (Block { ref = name, terms = trms ++ [Null False] }) (m_semi >> return Block { ref = name, terms = trms })

runParse :: Parser Run
runParse =  (do
                try $ m_reserved "debug"
                n <-  m_identifier
                return $ Debug n) <|>
            (do
                try $ m_reserved "run"
                n <- m_identifier
                return $ Run n)

pulParse :: Parser Pul
pulParse = m_whiteSpace >> handler <* eof
  where handler = do { bloks <- manyTill blockParse (try $ lookAhead $ ((m_reservedOp "run") <|> m_reserved "debug"))
                     ; rn    <- runParse
                     ; return $ Pul { blocks = bloks, run = rn }
                     }
